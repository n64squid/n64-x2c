<?php

function ca($alpha) {
	$alpha = ($alpha - 127) * -2;
	$alpha = ($alpha==254)?255:$alpha;
	return ($alpha);
}
function rgba16 ($t) {
	$r = hexdec(substr($t,0,2));
	$g = hexdec(substr($t,2,2));
	$b = hexdec(substr($t,4,2));
	$a = hexdec(substr($t,6,2));
	$n = dechex((floor($r/8)<<11) + (floor($g/8)<<6) + (floor($b/8)<<1) + round($a/256));
	while (strlen($n)<4){
		$n = '0'.$n;
	}
	return ('0x'.$n);
}
function getimgrarray($img,$name,$force='ci4'){

	//General settings
	$image = imagecreatefrompng($img);
	$s = getimagesize($img);
	$w = $s[0];
	$h = $s[1];
	if($w>32||$h>32){
		return('tb');
	}
	
	// Get texture array
	// This makes $texture into an array containing the 32-bit texture in hex format
	$rawtexture = array();
	for($y=0;$y<$h;$y++){
		for($x=0;$x<$w;$x++){
			$rgb = imagecolorat($image, $x, $y);
			$c = imagecolorsforindex($image,$rgb);
			$n = dechex(($c['red']<<24) + ($c['green']<<16) + ($c['blue']<<8) + ca($c['alpha']));
			while (strlen($n)<8){
				$n = '0'.$n;
			}
			array_push($rawtexture, $n);
		}
	}
	
	// Get colour index
	$ci = array_values(array_unique($rawtexture));
	
	// Check for alpha
	$a32 = false;
	foreach ($ci as $c) {
		if (substr($c,6,2) != "00" && substr($c,6,2) != "ff") {
			$a32 = true; // There is a non 1 or 0 alpha value
		}
	}
	
	// export hex values
	$out = "/*This file was made by the X2C file converter at https://n64squid.com/x2c/*/

/*Please visit for more information about how to make your own hombrew games.*/
	
";
	if (($a32 && !$force) || $force == 'rgba32') {
		// use 32-bit RGBA
		$out.="/*This is a 32-bit RGBA texture.*/".PHP_EOL."unsigned int ".$name."[] = {";
		foreach ($rawtexture as $k => $t) {
			if (!(($k) % $w)) {
				$out .= PHP_EOL."	";
			}
			$out .= '0x'.$t.', ';
		}
		$out .= PHP_EOL."};";
	} else if ((count($ci)<=16 && !$force) || $force == 'ci4') {
		// use 4-bit CI textures
		$out.="/*This is a 4-bit Colour Index texture.*/".PHP_EOL."unsigned char ".$name."[] = {";
		$s=(int)0;
		foreach($rawtexture as $key => $t){
			foreach($ci as $k => $c) {
				if($t == $c) {
					if (gettype($s)=='string'){
						$out .= '0x'.dechex($s).dechex($k).', ';
						$s=(int)0;
					} else {
						$s=(string)$k;
					}
					break;
				}
			}
			if (!(round($key) % $w)) {
				$out .= PHP_EOL."	";
			}
		}
		$out .= PHP_EOL."};";
		$out.=PHP_EOL.PHP_EOL."/*The colour index for the above texture.*/".PHP_EOL."unsigned short ".$name."_lut[] = {".PHP_EOL."	";
		foreach ($ci as $c) {
			$out .= rgba16($c).', ';
		}
		$out .= PHP_EOL."};";
	} else if ((count($ci)<=256 && !$force) || $force == 'ci8') {
		// use 8-bit CI textures
		$out.="/*This is an 8-bit Colour Index texture.*/".PHP_EOL."unsigned char ".$name."[] = {";
	} else {
		$out.="/*This is a 16-bit RGBA texture.*/".PHP_EOL."unsigned short ".$name."[] = {";
		foreach ($rawtexture as $k => $t) {
			if (!(($k) % $w)) {
				$out .= PHP_EOL."	";
			}
			$out .= rgba16($t).', ';
		}
		$out .= PHP_EOL."};";
	}
	
	
	return($out);
}

?>