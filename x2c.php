<?php
/**
 * Plugin Name: X 2 C
 * Plugin URI: https://n64squid.com
 * Description: Converts 3D model databases to C for N64 programming
 * Version: 0
 * Author: N64 Squid
 * Author URI: https://n64squid.com
 * License: GPL2
 */
include("img.php");
add_shortcode('x2c', 'toc');


if (isset($_FILES["x2cdb"])) {
	$dl = $_POST["x2cdl"] ? $_POST["x2cdl"] : '';
	$out.='<div id="id5918" class="collapseomatic colomat-visited" tabindex="" title="Transcript">File uploaded</div>
	<div id="target-id5918" class="collapseomatic_content " style="display: none;">
	<pre>'.var_export($_FILES["x2cdb"],true).'</pre>';
	$myfile = file_get_contents($_FILES["x2cdb"]['tmp_name']);
	$out.='<pre>'.$myfile.'</pre></div>';
	preg_match('/\..+$/',$_FILES["x2cdb"]['name'],$match);
	if ($match[0] == '.ply') {
		$out.='<pre>It\'s a ply.</pre>';
		$out .= '<pre>';
		include('ply.php');
		$out .= '</pre>';
	} else if ($match[0] == '.obj') {
		$out.='<pre>It\'s an obj.</pre>';
	} else if ($match[0] == '.png') {
		$filename = substr($_FILES["x2cdb"]['name'],0,strlen($_FILES["x2cdb"]['name'])-4);
		$str = getimgrarray($_FILES["x2cdb"]['tmp_name'],$filename);
		if($str!='tb'){
			/*header('Content-Disposition: attachment; filename="'.$filename.'.h"');
			header('Content-Type: text/plain');
			header('Content-Length: ' . strlen($str));
			header('Connection: close');*/
			
			echo '<pre>';
			var_dump($str);
		} else {
			echo 'Max image size: 32x32 px.';
		}
		exit;
	} else {
		$out.='<strong>Filetype not supported. Please choose an obj (wavefront) or ply (polygon) file format.</strong>';
	}
	
}
function toc(){
	$out = "";
	
		if (isset($_FILES["x2cdb"])) {
		$dl = $_POST["x2cdl"] ? $_POST["x2cdl"] : '';
		$out.='<div id="id5918" class="collapseomatic colomat-visited" tabindex="" title="Transcript">File uploaded</div>
		<div id="target-id5918" class="collapseomatic_content " style="display: none;">
		<pre>'.var_export($_FILES["x2cdb"],true).'</pre>';
		$myfile = file_get_contents($_FILES["x2cdb"]['tmp_name']);
		$out.='<pre>'.$myfile.'</pre></div>';
		preg_match('/\..+$/',$_FILES["x2cdb"]['name'],$match);
		if ($match[0] == '.ply') {
			$out.='<pre>It\'s a ply.</pre>';
			$out .= '<pre>';
			include('ply.php');
			$out .= '</pre>';
		} else if ($match[0] == '.obj') {
			$out.='<pre>It\'s an obj.</pre>';
		} else if ($match[0] == '.png') {
			$filename = substr($_FILES["x2cdb"]['name'],0,strlen($_FILES["x2cdb"]['name'])-4);
			$str = getimgrarray($_FILES["x2cdb"]['tmp_name'],$filename);
			if($str=='tb'){
				header('Content-Disposition: attachment; filename="'.$filename.'.h"');
				header('Content-Type: text/plain');
				header('Content-Length: ' . strlen($str));
				header('Connection: close');
				echo $str;
				exit;
			} else {
				echo 'Max image size: 16x16 px.';
			}
		} else {
			$out.='<strong>Filetype not supported. Please choose an obj (wavefront) or ply (polygon) file format.</strong>';
		}
		
	}
	
	$out .= '<br><form action="" method="post" enctype="multipart/form-data">  
  <div class="div-table">
  		<h3>X2C Converter</h3>
             <div class="div-table-row">
                <div  class="div-table-col"><input type="file" name="x2cdb" accept=".png"></div>
                <div class="div-table-col">&nbsp;</div>
             </div>
            <div class="div-table-row">
                <div class="div-table-col"><input type="submit" value=" Convert file "></div>
                <div class="div-table-col">&nbsp;</div>
            </div>

      </div>
</form><br>';
	return $out;
}

function x2c_css() {
    wp_enqueue_style('style-name', plugins_url()."/x2c/css.css" );
}
add_action( 'wp_enqueue_scripts', 'x2c_css' );