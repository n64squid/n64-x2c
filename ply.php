<?php
$filearray = explode(chr(10), $myfile);

// Divide the file into lines
$lines = array();
foreach ($filearray as $k => $f){
	if (!floatval($f)){
		unset($filearray[$k]);
	} else {
		array_push($lines,explode(' ',$f));
	}
}

// Store to an array of vertices and faces
$vertices = array();
$faces = array();
foreach ($lines as $k => $l) {
	if (count($l) >= 6){
		foreach ($l as $k2 => $l2) {
			$l[$k2] = floatval($lines[$k][$k2]);
		}
		array_push($vertices, $l);
	} else {
		foreach ($l as $k2 => $l2) {
			$l[$k2] = intval($lines[$k][$k2]);
		}
		array_push($faces, $l);
	}
}
// $out .= vertices array in C

$out .= 'Vtx_tn '.$dl.'_mdl[] = {<br>';
foreach ($vertices as $v) {
	$out .= "	{".$v[0].",".$v[1].",".$v[2].",0, 0, 0,".($v[6]?$v[6]:0).",".($v[7]?$v[7]:0).",".($v[8]?$v[8]:0).",255},
";
}
$out .= '};<br>';
// Connect the vertices
$out .= 'void '.$dl.'(Dynamic* dynamicp) {
	// Display list settings
	g'.($dl?'s':'').'SPMatrix('.($dl?$dl.'++,':'').'OS_K0_TO_PHYSICAL(&(dynamicp->projection)),G_MTX_PROJECTION|G_MTX_LOAD|G_MTX_NOPUSH);
	g'.($dl?'s':'').'SPMatrix('.($dl?$dl.'++,':'').'OS_K0_TO_PHYSICAL(&(dynamicp->modeling)),G_MTX_MODELVIEW|G_MTX_LOAD|G_MTX_NOPUSH);
	g'.($dl?'s':'').'DPPipeSync('.($dl?$dl.'++':'').');
	g'.($dl?'s':'').'DPSetCycleType('.($dl?$dl.'++,':'').'G_CYC_1CYCLE);
	g'.($dl?'s':'').'DPSetRenderMode('.($dl?$dl.'++,':'').'G_RM_AA_OPA_SURF, G_RM_AA_OPA_SURF2);
	g'.($dl?'s':'').'SPClearGeometryMode('.($dl?$dl.'++,':'').'0xFFFFFFFF);
	g'.($dl?'s':'').'SPSetGeometryMode('.($dl?$dl.'++,':'').'G_SHADE| G_SHADING_SMOOTH);
	// Vertices
';
	
// Load  vertices
$times = floor(count($vertices)/16);
$left = count($vertices) - ($times * 16);
for ($i = 0; $i < $times; $i++) {
    $out .= "	g".($dl?'s':'')."SPVertex(".($dl?$dl.'++,':'')."&(".$dl."_mdl[".($i*16)."]), 16, 0);
";
}
if ($left) {
    $out .= "	g".($dl?'s':'')."SPVertex(".($dl?$dl.'++,':'')."&(".$dl."_mdl[".($i*16)."]), ".$left.", 0);
";
}
$out .= '// Faces
';

foreach ($faces as $f){
	if ($f[0]==4) {
		$out .= "	g".($dl?'s':'')."SP2Triangles(".($dl?$dl.'++,':'')."".$f[1].",".$f[2].",".$f[3].",0,".$f[2].",".$f[1].",".$f[4].",0);
";
	} else {
		$out .= "	g".($dl?'s':'')."SP1Triangle(".($dl?$dl.'++,':'')."".$f[1].",".$f[2].",".$f[3].",0);
";
	}
}

$out .= '	g'.($dl?'s':'').'SPEndDisplayList('.($dl?$dl.'++':'').');
};
';
?>